import requests
import ftplib
from datetime import datetime, timedelta

import json
import pandas as pd
import matplotlib.pyplot as plt

now = datetime.now()
nowString = now.strftime("%Y-%m-%dT%H:%M:%SZ")
nowMinus3h = datetime.now() - timedelta(hours=3)
tomorrow = nowMinus3h + timedelta(hours=24)

nowMinus3hString = nowMinus3h.strftime("%Y-%m-%dT%H:%M:%SZ")
tomorrow = tomorrow.strftime("%Y-%m-%dT%H:%M:%SZ")

print("Script ran at: " + nowString)
# print("Currently is " + now_string)
# print("Tomorrow is " + tomorrow)

query = {#'start': '2021-09-17T12:00:59.999Z',
        'start': nowMinus3hString,
         'end': tomorrow}
response = requests.get('https://dashboard.elering.ee/api/nps/price', params=query)
jsonResponse = response.json()

# print(jsonResponse)

prices = jsonResponse["data"]["ee"]

for price in prices:
    price["timestamp"] = datetime.fromtimestamp(price["timestamp"]).strftime('%H:%M')

data = json.dumps(prices)

data = json.loads(data)
print(data)
dates = [i['timestamp'] for i in data]
values = [i['price'] for i in data]

print(dates)




avg = round(sum(values) / len(values))

df = pd.DataFrame({'dates': dates, 'values': values})
df['dates'] = [pd.to_datetime(i) for i in df['dates']]

# print(df.sort_values(by='dates'))


bar_colors = []
for val in values:
    if val < avg:
        bar_colors.append('green')
    else:
        bar_colors.append('red')

dictionary = {
      "type": "bar",
      "title": {
        "text": "Nordpool day ahead prices "+nowString,
        "fontSize": "20",
      },
      "legend": {
        "draggable": "true",
      },
      "scaleX": {
        # Set scale label
        "label": { "text": "Time of day" },
        # Convert text on scale indices
        "labels": dates
      },
      "scaleY": {
        # Scale label with unicode character
        "label": { "text": "€/MWh" }
      },
      "plot": {
        # Animation docs here:
        # https://www.zingchart.com/docs/tutorials/styling/animation#effect
        "animation": {
          "effect": "ANIMATION_EXPAND_BOTTOM",
          "method": "ANIMATION_STRONG_EASE_OUT",
          "sequence": "ANIMATION_BY_NODE",
          "speed": 275,
        },
          "styles": bar_colors
      },
      "series": [
        {
          # Plot 1 values, linear data
          "values": values
          #"text": "Week 1",
        }
      ]
    }

json_object = json.dumps(dictionary, indent=4)

# Writing to sample.json
with open("nordpool_data.json", "w") as outfile:
    outfile.write(json_object)

plt.xticks(rotation=45)
plt.bar(dates, values, color=bar_colors)

print("number of elements: ", len(values))

plt.savefig('nordpool.png')

session = ftplib.FTP('sn-69-6.tll07.zoneas.eu', 'mart.seedre.ee', 'WbaA75')
file = open('nordpool_data.json', 'rb')                  # file to send
session.storbinary('STOR /htdocs/nordpool/nordpool_data.json', file)     # send the file
file.close()                                    # close file and FTP
session.quit()