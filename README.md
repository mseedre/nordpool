# App to show nordpool prices

## Descriptions
Creates json and transfers it to seedre.ee/nordpool

## Installation
```
python3 -m venv /home/pi/nordpool/venv/
source /home/pi/nordpool/venv/bin/activate
pip install -r requirements.txt
```

## schedule it to run in crontab:
``
15 5,9,13,16,20 * * * /home/pi/nordpool/nordpool.sh >> /home/pi/nordpool_cron.log 2>&1
``